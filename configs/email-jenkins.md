# Configurar envio de email desde jenkins (email notification)

1. Hacer la configuración del archivo "google-smtp.md"

2. Accedemos a Jenkins -> Configurar el Sistema
3. Bajamos abajo del todo hasta la sección "Notificación por correo electrónico"
4. Seteamos las credenciales y abrimos la opción "Avanzado"
    - smtp.gmail.com
	- [√] Use SMTP Auth
	- prueba1.mtp@mail.com
	- Madrid01
	- [√] Habilitar check "Usar seguridad SSL"
	- Puerto 465
5. Ciquemaos sobre el check de "Probar la configuración enviando un correo de prueba"
    - Ponemos un correo valido para hacer la prueba y verificamos que efectivamente funciona recibiendo el correo