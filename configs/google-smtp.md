# Pasos para configurar la cuenta de gmail para enviar correos desde jenkins

1. Crear cuenta
2. Pinchamos en los 9 puntitos y vamos a "Cuenta"
3. Una vez dentro pintachos en el menu lateral sobre la opción "Seguridad"
4. Dentro de "Seguridad" bajamos hasta abajo del todo y pinchamos sobre "Acceso de aplicaciones poco seguras"
5. Dentro habilitamos el check de "Permitir el acceso de aplicaciones poco seguras"
6. Y listo, ya podemos enviar emails desde Jenkins.