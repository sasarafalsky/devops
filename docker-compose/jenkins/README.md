# Levantar Jenkins con docker-compose
```sh
    docker-compose up -d
```

# Levantar Jenkins con docker run
```sh
    1. docker network create jenkins

    2. docker volume create jenkins-docker-certs

    3. docker volume create jenkins-data

    4. docker volume create jenkins_prueba

    5. docker run -d -p 2376:2376 --privileged --network jenkins --network-alias docker -e DOCKER_TLS_CERTDIR=/certs -v jenkins-docker-certs:/certs/client -v jenkins-data:/var/jenkins_home docker:dind

    6. docker run -d -p 8088:8080 -p 50000:50000 --network jenkins -e DOCKER_HOST=tcp://docker:2376 -e DOCKER_CERT_PATH=/certs/client -e DOCKER_TLS_VERIFY=1 -v jenkins_prueba:/var/jenkins_home -v jenkins-docker-certs:/certs/client:ro jenkinsci/blueocean
```
