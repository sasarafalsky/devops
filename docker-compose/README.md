#######################################################################################################
# TODOS LOS COMANDOS SE DEBEN DE LANZAR DESDE LA CARPETA EN LA QUE SE ENCUENYTRA DOCKER-COMPOSE.YAML3 #
#######################################################################################################


# Levantar docker-compose
```sh
    docker-compose up -d
```

# Parar docker-compose
```sh
    docker-compose down
```

# Si tenemos varios servicios dentro y queremos levetar solo uno
```sh
    docker-compose up -d [NOMBRE DEL SERVICOS]
```

# Listar los servicios dentro de docker-compose
```sh
    docker-compose ps
```
