# DevOps: documentación Docker, docker-compose y kubernetes

# Folder schema
```sh
    \devops
        \docker
            README.md
        \docker-compose
            \jenkins
                docker-compose.yaml
            README.md
```